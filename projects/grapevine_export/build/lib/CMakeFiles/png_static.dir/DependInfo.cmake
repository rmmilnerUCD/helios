# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pc/Helios/core/lib/libpng/png.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/png.c.o"
  "/home/pc/Helios/core/lib/libpng/pngerror.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngerror.c.o"
  "/home/pc/Helios/core/lib/libpng/pngget.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngget.c.o"
  "/home/pc/Helios/core/lib/libpng/pngmem.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngmem.c.o"
  "/home/pc/Helios/core/lib/libpng/pngpread.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngpread.c.o"
  "/home/pc/Helios/core/lib/libpng/pngread.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngread.c.o"
  "/home/pc/Helios/core/lib/libpng/pngrio.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngrio.c.o"
  "/home/pc/Helios/core/lib/libpng/pngrtran.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngrtran.c.o"
  "/home/pc/Helios/core/lib/libpng/pngrutil.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngrutil.c.o"
  "/home/pc/Helios/core/lib/libpng/pngset.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngset.c.o"
  "/home/pc/Helios/core/lib/libpng/pngtrans.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngtrans.c.o"
  "/home/pc/Helios/core/lib/libpng/pngwio.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngwio.c.o"
  "/home/pc/Helios/core/lib/libpng/pngwrite.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngwrite.c.o"
  "/home/pc/Helios/core/lib/libpng/pngwtran.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngwtran.c.o"
  "/home/pc/Helios/core/lib/libpng/pngwutil.c" "/home/pc/Helios/projects/grapevine_export/build/lib/CMakeFiles/png_static.dir/pngwutil.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/pc/Helios/core/include"
  "/home/pc/Helios/core/lib/zlib"
  "/home/pc/Helios/core/lib/libpng"
  "lib/lib/zlib"
  "/home/pc/Helios/core/lib/libpng/../zlib"
  "lib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
