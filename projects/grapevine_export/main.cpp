#include "Context.h"
#include "CanopyGenerator.h" 

using namespace helios; //note that we are using the helios namespace so we can omit 'helios::' before names
     
int main(void){

  Context context;  //Declare the "Context" class
  Context* contextptr; 
  contextptr = &context;

  CanopyGenerator *canopy_generator = new CanopyGenerator(contextptr);

  vec3 origin(0,0,0);


  VSPGrapevineParameters params;

  params

  canopy_generator->grapevineVSP(params, origin);

  const char file_name[14] = "grapevine.obj";

  context.writeOBJ(file_name);

  return 1;

}
